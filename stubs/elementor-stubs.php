<?php

namespace Elementor {

	class Plugin {
		/**
		* Frontend.
		*
		* Holds the plugin frontend.
		*
		* @since 1.0.0
		* @access public
		*
		* @var Frontend
		*/
		public $frontend;

		/**
		* Template library manager.
		*
		* Holds the template library manager.
		*
		* @since 1.0.0
		* @access public
		*
		* @var TemplateLibrary\Manager
		*/
		public $templates_manager;

		/**
		* Documents manager.
		*
		* Holds the documents manager.
		*
		* @since 2.0.0
		* @access public
		*
		* @var Documents_Manager
		*/
		public $documents;

		/**
		* Editor.
		*
		* Holds the plugin editor.
		*
		* @since 1.0.0
		* @access public
		*
		* @var \Elementor\Core\Editor\Editor
		*/
		public $editor;

		/**
		* Instance.
		*
		* Ensures only one instance of the plugin class is loaded or can be loaded.
		*
		* @since 1.0.0
		* @access public
		* @static
		*
		* @return Plugin An instance of the class.
		*/
		public static function instance() {}
	}

	class Controls_Manager {
		/**
		* Content tab.
		*/
		const TAB_CONTENT = 'content';

		/**
		* Style tab.
		*/
		const TAB_STYLE = 'style';

		/**
		* Advanced tab.
		*/
		const TAB_ADVANCED = 'advanced';

		/**
		* Responsive tab.
		*/
		const TAB_RESPONSIVE = 'responsive';

		/**
		* Layout tab.
		*/
		const TAB_LAYOUT = 'layout';

		/**
		* Settings tab.
		*/
		const TAB_SETTINGS = 'settings';

		/**
		* Text control.
		*/
		const TEXT = 'text';

		/**
		* Number control.
		*/
		const NUMBER = 'number';

		/**
		* Textarea control.
		*/
		const TEXTAREA = 'textarea';

		/**
		* Select control.
		*/
		const SELECT = 'select';

		/**
		* Switcher control.
		*/
		const SWITCHER = 'switcher';

		/**
		* Button control.
		*/
		const BUTTON = 'button';

		/**
		* Hidden control.
		*/
		const HIDDEN = 'hidden';

		/**
		* Heading control.
		*/
		const HEADING = 'heading';

		/**
		* Raw HTML control.
		*/
		const RAW_HTML = 'raw_html';

		/**
		* Deprecated Notice control.
		*/
		const DEPRECATED_NOTICE = 'deprecated_notice';

		/**
		* Popover Toggle control.
		*/
		const POPOVER_TOGGLE = 'popover_toggle';

		/**
		* Section control.
		*/
		const SECTION = 'section';

		/**
		* Tab control.
		*/
		const TAB = 'tab';

		/**
		* Tabs control.
		*/
		const TABS = 'tabs';

		/**
		* Divider control.
		*/
		const DIVIDER = 'divider';

		/**
		* Color control.
		*/
		const COLOR = 'color';

		/**
		* Media control.
		*/
		const MEDIA = 'media';

		/**
		* Slider control.
		*/
		const SLIDER = 'slider';

		/**
		* Dimensions control.
		*/
		const DIMENSIONS = 'dimensions';

		/**
		* Choose control.
		*/
		const CHOOSE = 'choose';

		/**
		* WYSIWYG control.
		*/
		const WYSIWYG = 'wysiwyg';

		/**
		* Code control.
		*/
		const CODE = 'code';

		/**
		* Font control.
		*/
		const FONT = 'font';

		/**
		* Image dimensions control.
		*/
		const IMAGE_DIMENSIONS = 'image_dimensions';

		/**
		* WordPress widget control.
		*/
		const WP_WIDGET = 'wp_widget';

		/**
		* URL control.
		*/
		const URL = 'url';

		/**
		* Repeater control.
		*/
		const REPEATER = 'repeater';

		/**
		* Icon control.
		*/
		const ICON = 'icon';

		/**
		* Icons control.
		*/
		const ICONS = 'icons';

		/**
		* Gallery control.
		*/
		const GALLERY = 'gallery';

		/**
		* Structure control.
		*/
		const STRUCTURE = 'structure';

		/**
		* Select2 control.
		*/
		const SELECT2 = 'select2';

		/**
		* Date/Time control.
		*/
		const DATE_TIME = 'date_time';

		/**
		* Box shadow control.
		*/
		const BOX_SHADOW = 'box_shadow';

		/**
		* Text shadow control.
		*/
		const TEXT_SHADOW = 'text_shadow';

		/**
		* Entrance animation control.
		*/
		const ANIMATION = 'animation';

		/**
		* Hover animation control.
		*/
		const HOVER_ANIMATION = 'hover_animation';

		/**
		* Exit animation control.
		*/
		const EXIT_ANIMATION = 'exit_animation';
	}

	class Documents_Manager {
		/**
		* Get document.
		*
		* Retrieve the document data based on a post ID.
		*
		* @since 2.0.0
		* @access public
		*
		* @param int  $post_id    Post ID.
		* @param bool $from_cache Optional. Whether to retrieve cached data. Default is true.
		*
		* @return false|Document Document data or false if post ID was not entered.
		*/
		public function get( $post_id, $from_cache = true ) {}
	}

	abstract class Controls_Stack {
		/**
		* Register controls.
		*
		* Used to add new controls to any element type. For example, external
		* developers use this method to register controls in a widget.
		*
		* Should be inherited and register new controls using `add_control()`,
		* `add_responsive_control()` and `add_group_control()`, inside control
		* wrappers like `start_controls_section()`, `start_controls_tabs()` and
		* `start_controls_tab()`.
		*
		* @access protected
		*/
		protected function _register_controls() {}

		/**
		* Add new control to stack.
		*
		* Register a single control to allow the user to set/update data.
		*
		* This method should be used inside `register_controls()`.
		*
		* @since 1.4.0
		* @access public
		*
		* @param string $id      Control ID.
		* @param array  $args    Control arguments.
		* @param array  $options Optional. Control options. Default is an empty array.
		*
		* @return bool True if control added, False otherwise.
		*/
		public function add_control( $id, array $args, $options = [] ) {}

		/**
		* Add new group control to stack.
		*
		* Register a set of related controls grouped together as a single unified
		* control. For example grouping together like typography controls into a
		* single, easy-to-use control.
		*
		* @since 1.4.0
		* @access public
		*
		* @param string        $group_name Group control name.
		* @param array<mixed>  $args       Group control arguments. Default is an empty array.
		* @param array<mixed>  $options    Optional. Group control options. Default is an
		*                                  empty array.
		*/
		final public function add_group_control( $group_name, array $args = [], array $options = [] ) {}

		/**
		* Add new responsive control to stack.
		*
		* Register a set of controls to allow editing based on user screen size.
		* This method registers one or more controls per screen size/device, depending on the current Responsive Control
		* Duplication Mode. There are 3 control duplication modes:
		* * 'off' - Only a single control is generated. In the Editor, this control is duplicated in JS.
		* * 'on' - Multiple controls are generated, one control per enabled device/breakpoint + a default/desktop control.
		* * 'dynamic' - If the control includes the `'dynamic' => 'active' => true` property - the control is duplicated,
		*               once for each device/breakpoint + default/desktop.
		*               If the control doesn't include the `'dynamic' => 'active' => true` property - the control is not duplicated.
		*
		* @since 1.4.0
		* @access public
		*
		* @param string        $id      Responsive control ID.
		* @param array<mixed>  $args    Responsive control arguments.
		* @param array<mixed>  $options Optional. Responsive control options. Default is
		*                               an empty array.
		*/
		final public function add_responsive_control( $id, array $args, $options = [] ) {}

		/**
		* Start controls section.
		*
		* Used to add a new section of controls. When you use this method, all the
		* registered controls from this point will be assigned to this section,
		* until you close the section using `end_controls_section()` method.
		*
		* This method should be used inside `register_controls()`.
		*
		* @since 1.4.0
		* @access public
		*
		* @param string $section_id Section ID.
		* @param array  $args       Section arguments Optional.
		*/
		public function start_controls_section( $section_id, array $args = [] ) {}

		/**
		* End controls section.
		*
		* Used to close an existing open controls section. When you use this method
		* it stops adding new controls to this section.
		*
		* This method should be used inside `register_controls()`.
		*
		* @since 1.4.0
		* @access public
		*/
		public function end_controls_section() {}

		/**
		* Start controls tab.
		*
		* Used to add a new tab inside a group of tabs. Use this method before
		* adding new individual tabs using `start_controls_tab()`.
		* Each tab added after this point will be assigned to this group of tabs,
		* until you close it using `end_controls_tab()` method.
		*
		* This method should be used inside `register_controls()`.
		*
		* @since 1.4.0
		* @access public
		*
		* @param string $tab_id Tab ID.
		* @param array  $args   Tab arguments.
		*/
		public function start_controls_tab( $tab_id, $args ) {}

		/**
		* End controls tab.
		*
		* Used to close an existing open controls tab. When you use this method it
		* stops adding new controls to this tab.
		*
		* This method should be used inside `register_controls()`.
		*
		* @since 1.4.0
		* @access public
		*/
		public function end_controls_tab() {}

		/**
		* Start controls tabs.
		*
		* Used to add a new set of tabs inside a section. You should use this
		* method before adding new individual tabs using `start_controls_tab()`.
		* Each tab added after this point will be assigned to this group of tabs,
		* until you close it using `end_controls_tabs()` method.
		*
		* This method should be used inside `register_controls()`.
		*
		* @since 1.4.0
		* @access public
		*
		* @param string $tabs_id Tabs ID.
		* @param array  $args    Tabs arguments.
		*/
		public function start_controls_tabs( $tabs_id, array $args = [] ) {}

		/**
		* End controls tabs.
		*
		* Used to close an existing open controls tabs. When you use this method it
		* stops adding new controls to this tabs.
		*
		* This method should be used inside `register_controls()`.
		*
		* @since 1.4.0
		* @access public
		*/
		public function end_controls_tabs() {}
	}

	class Group_Control_Typography {
		/**
		* @return string Control type.
		*/
		public static function get_type() {}
	}

	class Group_Control_Box_Shadow {
		/**
		* @return string Control type.
		*/
		public static function get_type() {}
	}

	class Group_Control_Border {
		/**
		* @return string Control type.
		*/
		public static function get_type() {}
	}

	abstract class Document extends Controls_Stack {
		/**
		* Is built with Elementor.
		*
		* Check whether the post was built with Elementor.
		*
		* @since 2.0.0
		* @access public
		*
		* @return bool Whether the post was built with Elementor.
		*/
		public function is_built_with_elementor() {}
	}

	abstract class Element_Base extends Controls_Stack {
		/**
		* Get script dependencies.
		*
		* Retrieve the list of script dependencies the element requires.
		*
		* @since 1.3.0
		* @access public
		*
		* @return array Element scripts dependencies.
		*/
		public function get_script_depends() {}

		/**
		* Get style dependencies.
		*
		* Retrieve the list of style dependencies the element requires.
		*
		* @since 1.9.0
		* @access public
		*
		* @return array Element styles dependencies.
		*/
		public function get_style_depends() {}

		/**
		* Get settings for display.
		*
		* Retrieve all the settings or, when requested, a specific setting for display.
		*
		* Unlike `get_settings()` method, this method retrieves only active settings
		* that passed all the conditions, rendered all the shortcodes and all the dynamic
		* tags.
		*
		* @since 2.0.0
		* @access public
		*
		* @param string $setting_key Optional. The key of the requested setting.
		*                            Default is null.
		*
		* @return mixed The settings.
		*/
		public function get_settings_for_display( $setting_key = null ) {}
	}

	abstract class Widget_Base extends Element_Base {
		/**
		* Widget base constructor.
		*
		* Initializing the widget base class.
		*
		* @since 1.0.0
		* @access public
		*
		* @throws \Exception If arguments are missing when initializing a full widget
		*                   instance.
		*
		* @param array      $data Widget data. Default is an empty array.
		* @param array|null $args Optional. Widget default arguments. Default is null.
		*/
		public function __construct( $data = [], $args = null ) {}
	}

	class Widgets_Manager {
		/**
		* Register a new widget type.
		*
		* @param \Elementor\Widget_Base $widget_instance Elementor Widget.
		*
		* @return true True if the widget was registered.
		* @since 3.5.0
		* @access public
		*
		*/
		public function register( Widget_Base $widget_instance ) {}
	}

	class Frontend {
		/**
		* Registers styles.
		*
		* Registers all the frontend styles.
		*
		* Fired by `wp_enqueue_scripts` action.
		*
		* @since 1.2.0
		* @access public
		*/
		public function register_styles() {}

		/**
		* Registers scripts.
		*
		* Registers all the frontend scripts.
		*
		* Fired by `wp_enqueue_scripts` action.
		*
		* @since 1.2.1
		* @access public
		*/
		public function register_scripts() {}

		/**
		* Enqueue styles.
		*
		* Enqueue all the frontend styles.
		*
		* Fired by `wp_enqueue_scripts` action.
		*
		* @since 1.0.0
		* @access public
		*/
		public function enqueue_styles() {}

		/**
		* Enqueue scripts.
		*
		* Enqueue all the frontend scripts.
		*
		* @since 1.0.0
		* @access public
		*/
		public function enqueue_scripts() {}


		/**
		* Retrieve builder content for display.
		*
		* Used to render and return the post content with all the Elementor elements.
		*
		* @since 1.0.0
		* @access public
		*
		* @param int $post_id The post ID.
		*
		* @param bool $with_css Optional. Whether to retrieve the content with CSS
		*                       or not. Default is false.
		*
		* @return string The post content.
		*/
		public function get_builder_content_for_display( $post_id, $with_css = false ) {}
	}

}

namespace Elementor\Core\Editor {

	class Editor {
		/**
		* Whether the edit mode is active.
		*
		* Used to determine whether we are in the edit mode.
		*
		* @since 1.0.0
		* @access public
		*
		* @param int $post_id Optional. Post ID. Default is `null`, the current
		*                     post ID.
		*
		* @return bool Whether the edit mode is active.
		*/
		public function is_edit_mode( $post_id = null ) {}
	}

}

namespace Elementor\TemplateLibrary {

	class Manager {
		/**
		* Get templates.
		*
		* Retrieve all the templates from all the registered sources.
		*
		* @param array<string> $filter_sources
		*
		* @return array<mixed>
		*/
		public function get_templates( $filter_sources = [] ) {}
	}

}
